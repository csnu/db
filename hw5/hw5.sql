-- Question 1
-- D.ESSN = E.SSN to ensure the dependent for some employee
-- D.Gender = 'F' for returning female dependents
select E.LName, D.Name as 'Dependent Name' , D.BDate
from EMPLOYEE as E
join DEPENDENT as D
where D.Gender = 'F'
and D.ESSN = E.SSN;

-- Question 2
-- W.ESSN = E.SSN to find certain employee's hours
update WORKS_ON as W
join EMPLOYEE as E
set W.Hours = W.Hours - 1
where W.ESSN = E.SSN
and E.Salary < 80000;



-- Question 3
-- COUNT() function called to count number of dependents
select E.LName, COUNT( D.ESSN ) as '# of Dependents' from EMPLOYEE as E
join DEPENDENT as D
on D.ESSN = E.SSN
GROUP BY D.ESSN;


-- Question 4
-- deleting project 7 from WORKS_ON table
delete from WORKS_ON where WORKS_ON.PNumber = 7;

-- Question 5
-- SUM() function called to sum all the hours for certain project
select P.PName as 'Project Name', SUM(W.Hours) as 'Total Hours', COUNT(W.ESSN) as 'Employee Count'
from WORKS_ON as W
join PROJECT as P
where W.PNumber = P.PNumber
group by P.PName;

-- Question 6
-- returning number of departments by counting all the department name using COUNT() function
select COUNT(DName) as '# of Departments' from DEPARTMENT;

-- Question 7
-- W.ESSN is null means that no employers working in the project
select P.PName from PROJECT as P
join WORKS_ON as W
where P.PNumber = W.PNumber
and W.ESSN is null;

-- Question 8
-- This query returns employees who has no supervisor
-- which means that their SupervisorSSN would be null
select E.LName from EMPLOYEE as E
where E.SupervisorSSN is null;

-- Question 9
-- Department spent money is the total sum of all the employees working in that department
select D.DName, SUM( E.Salary ) as 'Money Spent'
from DEPARTMENT as D
join EMPLOYEE as E
where E.DNumber = D.DNumber
group by D.DName;

-- Question 10
-- counts only distinct supervisorSSN because there would many repetitive supervisorSSNs
select COUNT(DISTINCT E.SupervisorSSN) as '# of Supervisors' from EMPLOYEE as E;

-- Question 11
-- This query returns employees names and total hours using ESSN column in WORKS_ON table
select E.FName, E.LName, SUM( W.Hours ) as 'Total Hours'
from EMPLOYEE as E
join WORKS_ON as W
where W.ESSN = E.SSN
group by E.LName;

-- Question 12
-- selecting Maximum Hour Spent using Max() function
SELECT MAX(max_hour) as 'Max Hour Spent'
FROM (SELECT SUM(Hours) AS max_hour
FROM WORKS_ON
GROUP BY ESSN
) WO;


-- Question 13
-- This query find Manager of the deparment using MgrSSN column and
-- returns money spent by departments which is calculated by summing employee salary in the department
select D.DNumber, D.DName, Mgr.LName as 'Mgr LName', SUM(E.Salary) as 'Total Money Spent'
from DEPARTMENT as D
join EMPLOYEE as Mgr
join EMPLOYEE as E
where Mgr.SSN = D.MgrSSN
and E.DNumber = D.DNumber
group by D.DNumber
order by SUM( E.Salary );

-- Question 14
-- First find dependents and count them using COUNT() function
-- Order by descending number of dependents
select E.LName, COUNT( D.ESSN ) as '# of Dependents'
from EMPLOYEE as E
join DEPENDENT as D
on D.ESSN = E.SSN
GROUP BY D.ESSN
order by COUNT( D.ESSN ) desc;

-- Question 15
-- Find dependents for certain employee
-- and return their age difference using DATEDIFF() function (returns in days)
-- delete that days to 365.2425 and get years
-- ABS() FLOOR() needed to surpass certain obstacles
select E.LName, D.Name, FLOOR(ABS(DATEDIFF(E.BDate, D.BDate))/365.2425) as 'Age Difference'
from EMPLOYEE as E
join DEPENDENT as D
where D.ESSN = E.SSN
and D.Name like 'A%';

-- Question 16
-- Find departments and their location using DEPT_Location table
-- only accept locations that are more than two
SELECT Dep.DName, E1.FName, COUNT(DL.DNumber) AS '# of Locations'
FROM DEPARTMENT Dep
JOIN EMPLOYEE E1
ON Dep.MgrSSN = E1.SSN
JOIN DEPT_LOCATION DL
ON Dep.DNumber = DL.DNumber
GROUP BY DL.DNumber
HAVING COUNT(DL.DNumber) > 2;



-- Question 17
-- finds supervisor for some employee if that employee is not male then count increments
select SV.LName, SV.SSN, COUNT(E.SSN) as '# of Employees'
from EMPLOYEE as SV
join EMPLOYEE as E
where SV.SSN = E.SupervisorSSN
and E.Gender <> 'M'
group by SV.LName;



-- Question 18
-- Finding employee with Maximum salary using Max() function
select E.LName, MAX(E.Salary) as 'Salary' from EMPLOYEE as E
where E.Salary = ( select MAX(Salary) from EMPLOYEE);

-- Extra
SELECT LName, total_time 
FROM (SELECT E1.LName, SUM(WO.Hours) AS total_time
FROM EMPLOYEE E1
JOIN WORKS_ON WO
ON WO.ESSN = E1.SSN
GROUP BY WO.ESSN) Changed_table
HAVING total_time = (SELECT MAX(total_time) FROM (SELECT E1.LName, SUM(WO.Hours) AS total_time
FROM EMPLOYEE E1
JOIN WORKS_ON WO
ON WO.ESSN = E1.SSN
GROUP BY WO.ESSN) Changed_table);