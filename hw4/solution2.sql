
-- Question 1
-- Need an outer join to return even the employees with no supervisors
-- A default inner join returns only employees with supervisor
-- Notice you can rename the columns to be more clear
select E1.LName as Name, E2.LName as Supervisor
from EMPLOYEE E1 left outer join EMPLOYEE E2
on E1.SupervisorSSN = E2.SSN;

-- Question 2
-- Selected more columns than required in order to check my work
-- only employees with supervisors are required so inner join is OK
select E1.FName, E1.LName as Name, E1.Salary, E2.FName, E2.LName as Supervisor, E2.Salary
from EMPLOYEE E1 join EMPLOYEE E2
where E1.SupervisorSSN = E2.SSN
and E2.Salary < E1.Salary;

-- Question 3
-- There is one person (in the given data) who works on 2 edu projects 
-- so the distinct keyword gets rid of that 1 duplicate person
select distinct W.ESSN
from WORKS_ON W, PROJECT P
where W.PNumber = P.PNumber
and P.PName like '%edu';

-- Question 4
-- joining all the employee projects with all supervisor projects 
-- and then finding a match
select W1.PNumber, E1.FName, E1.LName as Name, W2.PNumber, E2.FName, E2.LName as Supervisor
from EMPLOYEE E1 join EMPLOYEE E2, WORKS_ON W1, WORKS_ON W2
where E1.SupervisorSSN = E2.SSN
and E1.SSN = W1.ESSN
and E2.SSN = W2.ESSN
and W1.PNumber = W2.PNumber;

-- using join syntax may be a bit clearer (question 4)
select W1.PNumber, E1.FName, E1.LName as Name, W2.PNumber, E2.FName, E2.LName as Supervisor
from (EMPLOYEE E1 join WORKS_ON W1 on E1.SSN = W1.ESSN) join
     (EMPLOYEE E2 join WORKS_ON W2 on E2.SSN = W2.ESSN)
     on E1.SupervisorSSN = E2.SSN
where W1.PNumber = W2.PNumber;

-- Question 5
select FName, LName, Salary
from EMPLOYEE
order by Salary desc;

-- Question 6
-- the dependents are sorted youngest to oldest, switch to asc for the opposite
select FName, Lname as 'Employee', Name as 'Dependent', D.BDate
from EMPLOYEE E, DEPENDENT D
where E.SSN = D.ESSN
order by E.LName, D.BDate desc;

-- Question 7
-- only joins, no selection criteria
select P.PName, E.LName as 'DeptManager'
from EMPLOYEE E, DEPARTMENT D, PROJECT P
where E.SSN = D.MgrSSN
and D.DNumber = P.DNumber;

-- Question 8
-- there are 2 female supervisors in the data
-- only empployees that have supervisors can meet this criteria
select E1.FName, E1.LName as Name, E2.FName, E2.LName as Supervisor
from EMPLOYEE E1, EMPLOYEE E2
where E1.SupervisorSSN = E2.SSN
and E1.Gender = 'F'
and E2.Gender = 'F'
order by E2.LName;

-- Question 9
-- department managers, not supervisors
select E.Fname, E.Lname as 'Employee', M.FName, M.LName as 'DeptManager'
from EMPLOYEE E, DEPARTMENT D, EMPLOYEE M
where E.Dnumber = D.DNumber
and D.MgrSSN = M.SSN
and E.Gender = 'M'
and M.Gender = 'M'
order by E.LName;

-- Question 10
-- Changed the dependents' names to match their genders to help check
-- the data had mismatched named
-- outer join needed to get male employees with no dependents
select E1.Fname, E1.LName as Employee, D.Name as Dependent
from EMPLOYEE E1 left outer join DEPENDENT D on E1.SSN = D.ESSN
where E1.Gender = 'M'
or D.Gender = 'M';