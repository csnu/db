-- Problem 1
SELECT E1.LName,
       E2.LName as SupervisorName
FROM EMPLOYEE AS E1
JOIN EMPLOYEE AS E2 
  ON E2.SSN = E1.SupervisorSSN;




-- Problem 2
SELECT E1.LName,
       E2.LName AS SupervisorName
FROM EMPLOYEE AS E1
JOIN EMPLOYEE AS E2 
  ON E2.SSN = E1.SupervisorSSN AND E2.Salary < E1.Salary;




-- Problem 3
SELECT E1.SSN
FROM EMPLOYEE AS E1 
JOIN PROJECT AS P1
ON E1.DNumber = P1.DNumber
WHERE P1.PName LIKE '%edu';




-- Problem 4
SELECT DISTINCT 
     E1.LName,
       E2.LName as SupName
FROM EMPLOYEE AS E1
JOIN EMPLOYEE AS E2 
  ON E2.SSN = E1.SupervisorSSN
JOIN WORKS_ON AS w
ON w.ESSN = E1.SSN;




-- ----------------- Problem 5 --------------------------
select LName, Salary from EMPLOYEE order by Salary DESC;
-- ----------------- end of Problem 5 --------------------------



-- Problem 6
select E1.FName, E1.LName, D.Name, D.BDate
from EMPLOYEE as E1
join DEPENDENT as D
on E1.SSN = D.ESSN
order by E1.LName, D.BDate asc;




-- Problem 7
select P.PName, E.LName as DepManager
from PROJECT as P
join DEPARTMENT as D
join EMPLOYEE as E
on P.DNumber = D.DNumber and D.MgrSSN = E.SSN;




-- Problem 8
SELECT E1.FName, E1.LName,
       E2.LName as SupervisorName
FROM EMPLOYEE AS E1
JOIN EMPLOYEE AS E2 
  ON E2.SSN = E1.SupervisorSSN
  where E2.Gender = 'F' and E1.Gender = 'F'
  order by E2.LName asc;
  
  
  
  
  
-- Problem 9
select E1.FName, E1.LName, DManager.LName
from EMPLOYEE as E1
join EMPLOYEE as DManager
join DEPARTMENT as D
on E1.DNumber = D.DNumber and D.MgrSSN = DManager.SSN
WHERE E1.Gender = 'M' and DManager.Gender ='M'
order by DManager.LName asc;




-- Problem 10
SELECT DISTINCT E1.FName, E1.LName
FROM EMPLOYEE AS E1
JOIN DEPENDENT as D
ON E1.SSN = D.ESSN
WHERE E1.Gender = 'M' OR D.Gender = 'M';