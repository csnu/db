-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema EmployeeDB
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bbay1ia6y6wvssmwldks` ;

-- -----------------------------------------------------
-- Schema EmployeeDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bbay1ia6y6wvssmwldks` DEFAULT CHARACTER SET utf8 ;
USE `bbay1ia6y6wvssmwldks` ;

-- -----------------------------------------------------
-- Table `EmployeeDB`.`DEPARTMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`DEPARTMENT` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (
  `DName` VARCHAR(30) NOT NULL,
  `DNumber` INT NOT NULL,
  `MgrStartDate` DATE NULL,
  `MgrSSN` CHAR(11) NULL,
  PRIMARY KEY (`DNumber`),
  CONSTRAINT `fk_DEPARTMENT_EMPLOYEE1`
    FOREIGN KEY (`MgrSSN`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`SSN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `DName_UNIQUE` ON `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (`DName` ASC) VISIBLE;

CREATE INDEX `fk_DEPARTMENT_EMPLOYEE1_idx` ON `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (`MgrSSN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`EMPLOYEE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`EMPLOYEE` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (
  `FName` VARCHAR(30) NULL,
  `LName` VARCHAR(30) NULL,
  `SSN` CHAR(11) NOT NULL,
  `BDate` DATE NULL,
  `Address` VARCHAR(45) NULL,
  `Gender` CHAR(1) NULL,
  `Salary` INT NULL,
  `DNumber` INT NOT NULL,
  `SupervisorSSN` CHAR(11) NULL,
  PRIMARY KEY (`SSN`),
  CONSTRAINT `fk_EMPLOYEE_DEPARTMENT`
    FOREIGN KEY (`DNumber`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (`DNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_EMPLOYEE_EMPLOYEE1`
    FOREIGN KEY (`SupervisorSSN`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`SSN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_EMPLOYEE_DEPARTMENT_idx` ON `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`DNumber` ASC) VISIBLE;

CREATE INDEX `fk_EMPLOYEE_EMPLOYEE1_idx` ON `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`SupervisorSSN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`DEPENDENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`DEPENDENT` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`DEPENDENT` (
  `Name` VARCHAR(30) NOT NULL,
  `Gender` CHAR(1) NULL,
  `BDate` DATE NULL,
  `Relationship` VARCHAR(30) NULL,
  `ESSN` CHAR(11) NOT NULL,
  PRIMARY KEY (`Name`, `ESSN`),
  CONSTRAINT `fk_DEPENDENT_EMPLOYEE1`
    FOREIGN KEY (`ESSN`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`SSN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_DEPENDENT_EMPLOYEE1_idx` ON `bbay1ia6y6wvssmwldks`.`DEPENDENT` (`ESSN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`PROJECT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`PROJECT` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`PROJECT` (
  `PNumber` INT NOT NULL,
  `PName` VARCHAR(45) NULL,
  `PLocation` VARCHAR(45) NULL,
  `DNumber` INT NULL,
  PRIMARY KEY (`PNumber`),
  CONSTRAINT `fk_PROJECT_DEPARTMENT1`
    FOREIGN KEY (`DNumber`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (`DNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_PROJECT_DEPARTMENT1_idx` ON `bbay1ia6y6wvssmwldks`.`PROJECT` (`DNumber` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`WORKS_ON`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`WORKS_ON` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`WORKS_ON` (
  `ESSN` CHAR(11) NOT NULL,
  `PNumber` INT NOT NULL,
  `Hours` INT NULL,
  PRIMARY KEY (`ESSN`, `PNumber`),
  CONSTRAINT `fk_EMPLOYEE_has_PROJECT_EMPLOYEE1`
    FOREIGN KEY (`ESSN`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`EMPLOYEE` (`SSN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_EMPLOYEE_has_PROJECT_PROJECT1`
    FOREIGN KEY (`PNumber`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`PROJECT` (`PNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_EMPLOYEE_has_PROJECT_PROJECT1_idx` ON `bbay1ia6y6wvssmwldks`.`WORKS_ON` (`PNumber` ASC) VISIBLE;

CREATE INDEX `fk_EMPLOYEE_has_PROJECT_EMPLOYEE1_idx` ON `bbay1ia6y6wvssmwldks`.`WORKS_ON` (`ESSN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`DEPT_LOCATION`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbay1ia6y6wvssmwldks`.`DEPT_LOCATION` ;

CREATE TABLE IF NOT EXISTS `bbay1ia6y6wvssmwldks`.`DEPT_LOCATION` (
  `Location` VARCHAR(30) NOT NULL,
  `DNumber` INT NOT NULL,
  PRIMARY KEY (`Location`, `DNumber`),
  CONSTRAINT `fk_DEPT_LOCATION_DEPARTMENT1`
    FOREIGN KEY (`DNumber`)
    REFERENCES `bbay1ia6y6wvssmwldks`.`DEPARTMENT` (`DNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_DEPT_LOCATION_DEPARTMENT1_idx` ON `bbay1ia6y6wvssmwldks`.`DEPT_LOCATION` (`DNumber` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `EmployeeDB`.`table1`
-- -----------------------------------------------------



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
