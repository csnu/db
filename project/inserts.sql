/*Hotels:*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`HOTEL` (`id`, `name`, `address`) VALUES ('156', 'Astana Plaza', 'Astana, Qabanbay Batyr Street, 53');
INSERT INTO `bbay1ia6y6wvssmwldks`.`HOTEL` (`id`, `name`, `address`) VALUES ('215', 'Shymkent Plaza', 'Shymkent, Abay Street, 211');

/*Room Types:*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM_TYPE` (`name`, `capacity`, `size`, `HOTEL_id`, `num_of_rooms`) VALUES ('Single', '4', '16', '156', '2');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM_TYPE` (`name`, `capacity`, `size`, `HOTEL_id`, `num_of_rooms`) VALUES ('Double', '8', '24', '156', '2');

INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM_TYPE` (`name`, `capacity`, `size`, `HOTEL_id`, `num_of_rooms`) VALUES ('Single', '5', '18', '215', '2');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM_TYPE` (`name`, `capacity`, `size`, `HOTEL_id`, `num_of_rooms`) VALUES ('Double', '9', '25', '215', '2');


/*categories:*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`CATEGORY` (`name`, `discount_percentage`) VALUES ('gold', '15');
INSERT INTO `bbay1ia6y6wvssmwldks`.`CATEGORY` (`name`, `discount_percentage`) VALUES ('default', '0');
INSERT INTO `bbay1ia6y6wvssmwldks`.`CATEGORY` (`name`, `discount_percentage`) VALUES ('silver', '10');
INSERT INTO `bbay1ia6y6wvssmwldks`.`CATEGORY` (`name`, `discount_percentage`) VALUES ('bronze', '5');

/*guests:*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST` (`guest_id`, `id_type`, `home_phone#`, `address`, `FName`, `LName`, `category`) VALUES ('2154678', 'Passport', '215-46-75', 'Astana, Turan Street, 22', 'Mona', 'Rizvi', 'gold');
INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST` (`guest_id`, `id_type`, `home_phone#`, `address`, `FName`, `LName`, `category`) VALUES ('1111111', 'Driving license', '666-66-66', 'Uralsk, Seifullin Street, 25', 'John', 'Dow', 'bronze');
INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST_Phone` VALUES ('778006317', 1111111);
/*rooms*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`, `is_occupied`) VALUES ('212', '2', '156', 'Double',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('311', '3', '156', 'Double',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('111', '1', '156', 'Single',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('113', '1', '156', 'Single',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('411', '4', '215', 'Double',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('512', '5', '215', 'Double',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('611', '6', '215', 'Single',NOW(),'no');
INSERT INTO `bbay1ia6y6wvssmwldks`.`ROOM` (`number`, `floor`, `hotel_id`, `room_type`,`cleaning_schedule`,`is_occupied`) VALUES ('211', '2', '215', 'Single',NOW(),'no');

/*services*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES` (`service_id`, `name_od_service`, `price`, `hotel_id`) VALUES ('12', 'Massage', '15', '156');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES` (`service_id`, `name_od_service`, `price`, `hotel_id`) VALUES ('13', 'Doctor', '40', '156');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES` (`service_id`, `name_od_service`, `price`, `hotel_id`) VALUES ('14', 'Car rent', '75', '156');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES` (`service_id`, `name_od_service`, `price`, `hotel_id`) VALUES ('15', 'Excursion', '10', '156');

/*seasons*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`SEASON` (`season_name`) VALUES ('winter');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SEASON` (`season_name`) VALUES ('default');


/*seasons_has_hotel*/

INSERT INTO `bbay1ia6y6wvssmwldks`.`SEASON_has_HOTEL` (`season`, `hotel_id`, `start_date`, `end_date`) VALUES ('winter', '156', '2020-12-01', '2020-12-31');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SEASON_has_HOTEL` (`season`, `hotel_id`, `start_date`, `end_date`) VALUES ('default', '156', '2020-01-01', '2020-11-30');


/*prices*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`PRICE` (`price`, `room_type`, `hotel_id`, `season`, `price_for_weekend`) VALUES ('65', 'Single', '156', 'default', '75');
INSERT INTO `bbay1ia6y6wvssmwldks`.`PRICE` (`price`, `room_type`, `hotel_id`, `season`, `price_for_weekend`) VALUES ('71', 'Single', '156', 'winter', '83');
INSERT INTO `bbay1ia6y6wvssmwldks`.`PRICE` (`price`, `room_type`, `hotel_id`, `season`, `price_for_weekend`) VALUES ('85', 'Double', '156', 'default', '94');
INSERT INTO `bbay1ia6y6wvssmwldks`.`PRICE` (`price`, `room_type`, `hotel_id`, `season`, `price_for_weekend`) VALUES ('91', 'Double', '156', 'winter', '116');



/*reservations:*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`RESERVATION` (`reservation_id`, `checkin_date`, `checkout_date`, `hotel_name`, `#of people`, `reservation_date`, `guest_id`, `total_nights`, `#_of_rooms`, `room_type`, `hotel_id`,`total_price_for_room(s)`, `total_price_for_services`, `total_price_with_discount`) VALUES ('666', '2020-10-15', '2020-10-18', 'Astana Plaza', '2', NOW(), '1111111', '3', '2', 'Double', 156, 600, 0, 600);
INSERT INTO `bbay1ia6y6wvssmwldks`.`RESERVATION` (`reservation_id`, `checkin_date`, `checkout_date`, `hotel_name`, `#of people`, `reservation_date`, `guest_id`, `total_nights`, `#_of_rooms`, `room_type`, `hotel_id`,`total_price_for_room(s)`, `total_price_for_services`, `total_price_with_discount`) VALUES ('667', '2020-11-15', '2020-11-18', 'Astana Plaza', '2', NOW(), '1111111', '3', '2', 'Single', 156, 300, 400, 700);



/*guest room history*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST_ROOM_HISTORY` (`guest_id`, `check_in_date`, `check_out_date`, `room_number`, `hotel_id`, `room_type`, `#_of_other_occupants`) VALUES ('1111111', '2020-10-13', '2020-10-17', '311', '156', 'Double', '1 - his sister, Joahn Dow');
