/*1.
a.
To find available rooms we find # of rooms of each room type and then number of reservations of this roomtype that intersects with our dates. 
If # of reservations is less than # of total rooms that means this roomtype is available.
To find total price we need to compute total price for weekdays and total price for wekeend days
Using special formula we can compute number of weekdays and weekend days in date period and multiplying by price/price_for_weekend 
find total price for weekdays/weekend
Using same algorithm find season that intersects our period of date and by joining it with price find prices and prices_for_weekend 
for every room type
To find average price divide total price by DATEDIFF(check_out_date, check_in_date)*/

/* set statements could be changed*/
SET @ci := '2020-12-15';
SET @co := '2020-12-23';
SET @hotel_id := 156;

select distinct rt.name,

(pr.price 
* (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1)) 
+ pr.price_for_weekend * 
(DATEDIFF(@co, @ci) - (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1)))
) as total_price, 

(pr.price 
* (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1))
+ pr.price_for_weekend * 
(DATEDIFF(@co, @ci) - (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1)))
)/DATEDIFF(@co, @ci) as average_price_for_night, 

rt.num_of_rooms - (
(select count(`reservation_id`)
from RESERVATION as rv
where (@ci <= rv.checkout_date and @co >= rv.checkin_date)
and rv.room_type = rt.name and rv.hotel_id = @hotel_id) * rv.`#_of_rooms`) as number_of_available_rooms
 
from ROOM_TYPE as rt
join PRICE as pr on pr.room_type = rt.name
join RESERVATION as rv on rv.room_type = rt.name
join HOTEL as h on h.id = rt.HOTEL_id
where rt.num_of_rooms > (

(select count(`reservation_id`) as num_of_reserv
from RESERVATION as rv
where (rv.checkin_date <= @co and rv.checkout_date >= @ci)
 and rv.room_type = rt.name) * rv.`#_of_rooms`)
 
 and pr.season = (
 select sh.season
 from SEASON_has_HOTEL as sh 
 where (sh.start_date <= @co and sh.end_date >= @ci)
 and sh.hotel_id = h.id)

 and h.id = @hotel_id;




/*b.
Our reservation system allow to reserve several rooms of only one room type so to complete these reservations we need to complete two inserts. Also our database cannot show separation of people between rooms (3 for one room 4 for another) so in reservation it will be 7 people in total.

It could be some limitations due to capacity of some rooms could be less than # of people in reservations but for this task we increased capacity of rooms
*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`RESERVATION` (`reservation_id`, `checkin_date`, `checkout_date`, `hotel_name`, `#of people`, 
`reservation_date`, `guest_id`, `total_nights`, `#_of_rooms`, `room_type`, `hotel_id`,`total_price_for_room(s)`, `total_price_for_services`) 
VALUES ('211', '2020-12-15', '2020-12-17', 'Astana Plaza', '2', NOW(), '2154678', '2', '1', 'Single', '156', 233, 0);

INSERT INTO `bbay1ia6y6wvssmwldks`.`RESERVATION` (`reservation_id`, `checkin_date`, `checkout_date`, `hotel_name`, `#of people`, 
`reservation_date`, `guest_id`, `total_nights`, `#_of_rooms`, `room_type`, `hotel_id`,`total_price_for_room(s)`, `total_price_for_services`)
 VALUES ('212', '2020-12-15', '2020-12-17', 'Astana Plaza', '7', NOW(), '2154678', '2', '2', 'Double', '156', 156, 0);

/* universal style to insert reservations*/

SET @id := 220;
SET @ci := '2020-12-15';
SET @co := '2020-12-17';
SET @rt := 'Single';
SET @num_of_rooms := 4;
SET @num_of_people := 2;
SET @guest_id := 2154678;
SET @hotel_id := 156;

INSERT INTO `bbay1ia6y6wvssmwldks`.`RESERVATION` (`reservation_id`, `checkin_date`, `checkout_date`, `hotel_name`, `#of people`,
 `reservation_date`, `guest_id`, `total_nights`, `#_of_rooms`, `room_type`, `hotel_id`, `total_price_for_room(s)`, `total_price_for_services`) 
VALUES (@id, @ci, @co, (select name from HOTEL where HOTEL.id = @hotel_id ), @num_of_people, NOW(), @guest_id, DATEDIFF(@co, @ci), 
@num_of_rooms, @rt, @hotel_id, (
select (pr.price * 
 (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1))
+ pr.price_for_weekend * 
(DATEDIFF(@co, @ci) - (5 * (DATEDIFF(@co, @ci) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(@ci) + WEEKDAY(@co) + 1, 1)))
) * @num_of_rooms from 
PRICE as pr
where pr.room_type = @rt and pr.season = (
 select sh.season
 from SEASON_has_HOTEL as sh 
 where (sh.start_date <= @co and sh.end_date >= @ci)
)), '0');

/*c.
/* groupping by guest id, however we select reservations by date they was made*/

select g.guest_id, SUM(rv.`total_price_for_room(s)`) * (100 - c.discount_percentage)/100 as total_disc_price
from GUEST as g 
join RESERVATION as rv on rv.guest_id = g.guest_id
join CATEGORY as c on g.category = c.name
where g.guest_id = @guest_id and date(rv.reservation_date) = curdate()
group by guest_id;

/*2.
a.
Suppose day of arrival is today and we need to find room that free and clean by roomtype
Free rooms by roomtype we find by attribute is occupied in ROOM table and setting attribute roomtype to specific roomtype (ex. ‘Double’)
Clean rooms we find by cleaning_schedule. Cleaning schedule shows when room was  cleaned last time. Room is clean if cleaning was made today, so that mean we need to find rooms that date of cleaning should be the same as date of arrival. 
*/
select r.number from
ROOM as r 
where r.room_type = 'Double' and r.is_occupied = 'no'
and date(r.cleaning_schedule) = curdate()
and r.hotel_id = 156;

/*b. 
John Smith is not registered in our system so firstly we need to insert him in GUEST and then in GUEST_ROOM_HISTORY
We decide to insert him into room 212 but it could be any free double room in system
*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST` (`guest_id`, `id_type`, `home_phone#`, `address`, `FName`, `LName`, `category`) VALUES 
('6134578', 'Passport', '555-55-55', 'London, Baker Street, 221B', 'John', 'Smith', 'default');

INSERT INTO `bbay1ia6y6wvssmwldks`.`GUEST_ROOM_HISTORY` (`guest_id`, `check_in_date`, `check_out_date`, `room_number`, `hotel_id`, `room_type`, 
`#_of_other_occupants`) VALUES 
('6134578', CURDATE(), '2020-12-16', '212', '156', 'Double', '1 - his wife, Maggie Smith');

UPDATE `bbay1ia6y6wvssmwldks`. `ROOM` SET `is_occupied` = 'yes'  where number = 212;





/*3.
Our database store all guest room history so we need only set room_number and date in check-in – check-out range. Also guest room history store some inforamtion about other occupants of this room.
*/
SET @date := '2020-10-15';
SET @rn := '311';

select g.FName, g.LName, g.address, g.`home_phone#` ,g.guest_id, g.id_type,  ph.`phone#`, grh.`#_of_other_occupants`
from GUEST as g
join GUEST_ROOM_HISTORY as grh on grh.guest_id = g.guest_id
join GUEST_Phone as ph on g.guest_id = ph.guest_id
where grh.room_number = @rn and (grh.check_in_date <= @date and grh.check_out_date >= @date) and grh.hotel_id = 156;

/*4.
a.
Story about all available services in special hotel are available in SERVICES table by selecting hotel_id to specific hotel where you were staying

Story about used services during reservation period are stored in SERVICES_DURING_RESERVATION_PERIOD table by selecting specific res_id and hotel_id

b.
Services are could be included in any reservation by adding them to  SERVICES_DURING_RESERVATION_PERIOD table and updating total_price_for_services in RESERVATION table
*/
INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES_DURING_RESERVATION_PERIOD` (`res_id`, `serv_id`, `hotel_id`, `datetime`) 
VALUES ('211', '12', '156', '2020-12-15-14-12');
INSERT INTO `bbay1ia6y6wvssmwldks`.`SERVICES_DURING_RESERVATION_PERIOD` (`res_id`, `serv_id`, `hotel_id`, `datetime`) 
VALUES ('211', '13', '156', '2020-12-16-18-11');


UPDATE RESERVATION SET `total_price_for_services` = (
select (SUM(s.price)) 
from SERVICES as s 
join SERVICES_DURING_RESERVATION_PERIOD as sr
where s.service_id = sr.serv_id and sr.res_id = 211) where `reservation_id` = 211;


/*c.
Firstly update to get total_price_with_discount 
*/
UPDATE RESERVATION SET `total_price_with_discount` = (total_price_for_services + `total_price_for_room(s)`) * (select * from (select (100 - c.discount_percentage)/100 
from GUEST as g 
join RESERVATION as rv on g.guest_id = rv.guest_id
join CATEGORY as c on c.name = g.category
where rv.reservation_id = 211) as x) where reservation_id = 211;

UPDATE RESERVATION SET `total_price_with_discount` = (total_price_for_services + `total_price_for_room(s)`) * (select * from (select (100 - c.discount_percentage)/100 
from GUEST as g 
join RESERVATION as rv on g.guest_id = rv.guest_id
join CATEGORY as c on c.name = g.category
where rv.reservation_id = 212) as x) where reservation_id = 212;

/*In our database reservation is store bill information so for separate reservations we make separate bills
*/
select rv.reservation_id, rv.`total_price_for_room(s)`, rv.total_price_for_services, rv.total_price_with_discount
from RESERVATION as rv 
where rv.reservation_id = 211;




/*For every bill we can show detailed bill by services that was used during reservation period
*/
select rv.reservation_id, s.name_od_service, s.price, rs.datetime
from SERVICES_DURING_RESERVATION_PERIOD as rs
join RESERVATION as rv on rv.reservation_id = rs.res_id
join SERVICES as s on s.service_id = rs.serv_id
where rv.reservation_id = 211;

/*5.
Update GUESTS with condition that category = silver, sum of total prices in bill is more than 1000 and check-in date after 2020-01-01
*/
UPDATE GUEST SET category = 'silver' where category = 'bronze' and guest_id = (select * from (select g.guest_id 
from GUEST as g 
where (select sum(rv.total_price_with_discount) 
from RESERVATION as rv
where g.guest_id = rv.guest_id and rv.checkin_date >= '2020-01-01') > 1000) as x);

